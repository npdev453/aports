# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=13.4.1
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/jbarlow83/OCRmyPDF"
arch="noarch"
license="MIT"
options="!check" # missing pytest modules
depends="
	python3
	py3-cffi
	py3-chardet
	py3-coloredlogs
	py3-img2pdf
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-reportlab
	py3-tqdm

	ghostscript
	jbig2enc
	leptonica
	pngquant
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="py3-setuptools py3-setuptools_scm py3-setuptools-scm-git-archive"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-xdist"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -e '/setuptools_scm/d' \
		-e "/use_scm_version/cversion='$pkgver'," \
		-i setup.py
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
074653c1d3f1394ab1e189722abeb7396622ed7c890dca47d83c51d00ec91ec28047074b5b118352e259cd7cfb82aa6179841779dfb244fa638723871138cc99  ocrmypdf-13.4.1.tar.gz
"
