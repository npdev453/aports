# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-markdown-it-py
pkgver=2.0.1
pkgrel=0
pkgdesc="Markdown parser, done right"
url="https://markdown-it-py.readthedocs.io/"
arch="noarch"
license="MIT"
depends="python3 py3-attrs py3-mdurl"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-benchmark"
source="https://github.com/executablebooks/markdown-it-py/archive/v$pkgver/py3-markdown-it-py-$pkgver.tar.gz"
builddir="$srcdir/markdown-it-py-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# fixture 'data_regression' not found
	# needs py3-pytest-regressions
	pytest -k 'not test_linkify' \
		--deselect=tests/test_tree.py::test_pretty \
		--deselect=tests/test_api/test_main.py::test_table_tokens \
		--deselect=tests/test_cmark_spec/test_spec.py::test_file \
		--deselect=tests/test_port/test_references.py::test_use_existing_env \
		--deselect=tests/test_port/test_references.py::test_store_labels \
		--deselect=tests/test_port/test_references.py::test_inline_definitions
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
fccf00de7e94986a29047da948d6ebb6304ae3645e79bbad480366c3aadbd417b95d96489d167a532eee3c2e17b67e22777224290564c7bcc17d72fec29b08df  py3-markdown-it-py-2.0.1.tar.gz
"
